import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
    // loadChildren: () => import('./pages/loading/loading.module').then(m => m.LoadingPageModule)
  },
  {
    path: 'beranda',
    loadChildren: () => import('./pages/beranda/beranda.module').then(m => m.BerandaPageModule)
  },
  {
    path: 'produk-detail',
    loadChildren: () => import('./pages/beranda/produk-detail/produk-detail.module').then(m => m.ProdukDetailPageModule)
  },
  {
    path: 'keranjang',
    loadChildren: () => import('./pages/keranjang/keranjang.module').then(m => m.KeranjangPageModule)
  },
  {
    path: 'pesanan',
    loadChildren: () => import('./pages/pesanan/pesanan.module').then(m => m.PesananPageModule)
  },
  {
    path: 'pesanan-riwayat',
    loadChildren: () => import('./pages/pesanan/pesanan-riwayat/pesanan-riwayat.module').then( m => m.PesananRiwayatPageModule)
  },
  {
    path: 'akun',
    loadChildren: () => import('./pages/akun/akun.module').then(m => m.AkunPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./pages/beranda/checkout/checkout.module').then(m => m.CheckoutPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'registrasi',
    loadChildren: () => import('./pages/login/registrasi/registrasi.module').then(m => m.RegistrasiPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
