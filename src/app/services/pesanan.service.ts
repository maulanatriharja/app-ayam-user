import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface Pesanan {
  id?: string,
  nota: string,
}

@Injectable({
  providedIn: 'root'
})
export class PesananService {

  private pesanan: Observable<Pesanan[]>;
  private pesananCollection: AngularFirestoreCollection<Pesanan>;

  constructor(
    private afs: AngularFirestore
  ) {
    this.pesananCollection = this.afs.collection<Pesanan>('pesanan');
    this.pesanan = this.pesananCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getpesanan(): Observable<Pesanan[]> {
    return this.pesanan;
  }

  getPesanan(id: string): Observable<Pesanan> {
    return this.pesananCollection.doc<Pesanan>(id).valueChanges().pipe(
      take(1),
      map(pesanan => {
        pesanan.id = id;
        return pesanan
      })
    );
  }

  addPesanan(pesanan: Pesanan): Promise<DocumentReference> {
    return this.pesananCollection.add(pesanan);
  }

  /* updatePesanan(pesanan: Pesanan): Promise<void> {
    return this.pesananCollection.doc(pesanan.id).update({
      nama_pesanan: pesanan.nama_pesanan,
      harga_modal: pesanan.harga_modal,
      harga_jual: pesanan.harga_jual,
      keterangan: pesanan.keterangan,
    });
  } */

  /* deletePesanan(id: string): Promise<void> {
    return this.pesananCollection.doc(id).delete();
  } */
}