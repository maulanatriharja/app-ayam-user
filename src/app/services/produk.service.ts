import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface Produk {
  id?: string,
  nama_produk: string,
  harga_modal: string,
  harga_jual: string,
  keterangan: string,
  foto: string
}

@Injectable({
  providedIn: 'root'
})
export class ProdukService {

  private produk: Observable<Produk[]>;
  private produkCollection: AngularFirestoreCollection<Produk>;

  constructor(
    private afs: AngularFirestore
  ) {
    this.produkCollection = this.afs.collection<Produk>('produk');
    this.produk = this.produkCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getproduk(): Observable<Produk[]> {
    return this.produk;
  }

  getProduk(id: string): Observable<Produk> {
    return this.produkCollection.doc<Produk>(id).valueChanges().pipe(
      take(1),
      map(produk => {
        produk.id = id;
        return produk
      })
    );
  }

  addProduk(produk: Produk): Promise<DocumentReference> {
    return this.produkCollection.add(produk);
  }

  updateProduk(produk: Produk): Promise<void> {
    return this.produkCollection.doc(produk.id).update({
      nama_produk: produk.nama_produk,
      harga_modal: produk.harga_modal,
      harga_jual: produk.harga_jual,
      keterangan: produk.keterangan,
    });
  }

  deleteProduk(id: string): Promise<void> {
    return this.produkCollection.doc(id).delete();
  }
}