import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'beranda',
        loadChildren: () => import('../pages/beranda/beranda.module').then(m => m.BerandaPageModule)
      },
      // {
      //   path: 'keranjang',
      //   loadChildren: () => import('../pages/keranjang/keranjang.module').then(m => m.KeranjangPageModule)
      // },
      {
        path: 'pesanan',
        loadChildren: () => import('../pages/pesanan/pesanan.module').then(m => m.PesananPageModule)
      },
      {
        path: 'akun',
        loadChildren: () => import('../pages/akun/akun.module').then(m => m.AkunPageModule)
      },
      {
        path: 'login',
        loadChildren: () => import('../pages/login/login.module').then(m => m.LoginPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/beranda',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/beranda',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class TabsPageRoutingModule { }
