import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AkunService } from '../services/akun.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: [ 'tabs.page.scss' ]
})
export class TabsPage {

  loggedin = false;

  constructor(
    public akunService: AkunService,
    public storage: Storage
  ) {
    this.akunService.obvAkun().subscribe(() => {
      this.ionViewDidEnter();
    });
  }

  ionViewDidEnter() {
    this.loggedin = false;

    this.storage.get('akun').then((val) => {
      if (val.id) {
        this.loggedin = true;
      } 
      // else {
      //   this.loggedin = false;
      // }
    });
  }

}
