import { Component, OnInit } from '@angular/core';
import { AnimationController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ProdukService, Produk } from '../../services/produk.service';
import { Observable } from 'rxjs';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';


@Component({
  selector: 'app-beranda',
  templateUrl: './beranda.page.html',
  styleUrls: [ './beranda.page.scss' ],
})
export class BerandaPage implements OnInit {

  produks: Observable<Produk[]>;

  total_bayar: number = 0;
  total_kuantiti: number = 0;

  pesanan: any = [];
  data: any = [];

  skl_array: any = [ 0, 1, 2, 3, 4, 5 ];
  sec:number=0;
  constructor(
    public animationCtrl: AnimationController, 
    public firebaseX: FirebaseX,
    public produkService: ProdukService,
    public router: Router
  ) {

  }

  ngOnInit() {
    this.firebaseX.getToken()
      .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
      .catch(error => console.error('Error getting token', error));
  }

  ionViewDidEnter() {
    this.data = [];
    this.total_bayar = 0;
    this.total_kuantiti = 0;

    // this.produks = this.produkService.getproduk();

    this.produkService.getproduk().subscribe(snap => {
      // this.data = snap;

      // alert(JSON.stringify(this.data[ 0 ]));

      for (let i = 0; i < snap.length; i++) {
        this.data.push({
          id: snap[ i ].id,
          nama_produk: snap[ i ].nama_produk,
          harga_modal: snap[ i ].harga_modal,
          harga_jual: snap[ i ].harga_jual,
          keterangan: snap[ i ].keterangan,
          kuantiti: 0,
        })
      }
    });

    if (this.total_kuantiti == 0) {
      this.animationCtrl.create()
        .addElement(document.querySelector('.ani-popup'))
        .duration(0)
        .fromTo('transform', 'translateY(100px)', 'translateY(100px)').play();
    }
  }

  tambah(val_id, val_harga) {
    if (this.total_kuantiti == 0) {
      this.animationCtrl.create()
        .addElement(document.querySelector('.ani-popup'))
        .duration(230)
        .fromTo('transform', 'translateY(100px)', 'translateY(0px)').play();
    }

    var id = this.data.findIndex(x => x.id == val_id);
    this.data[ id ].kuantiti += 1;
    this.total_kuantiti += 1;
    this.total_bayar += val_harga;
  }

  kurang(val_id, val_harga, val_kuantiti) {
    if (val_kuantiti > 0) {
      var id = this.data.findIndex(x => x.id == val_id);
      this.data[ id ].kuantiti -= 1;
      this.total_kuantiti -= 1;
      this.total_bayar -= val_harga;

      if (this.total_kuantiti == 0) {
        if (this.total_kuantiti == 0) {
          this.animationCtrl.create()
            .addElement(document.querySelector('.ani-popup'))
            .duration(230)
            .fromTo('transform', 'translateY(0px)', 'translateY(100px)').play();
        }
      }
    }
  }

  pesan() {
    this.pesanan = [];

    for (let i = 0; i < this.data.length; i++) {
      if (this.data[ i ].kuantiti > 0) {
        this.pesanan.push({
          id: this.data[ i ].id,
          nama_produk: this.data[ i ].nama_produk,
          harga_jual: this.data[ i ].harga_jual,
          kuantiti: this.data[ i ].kuantiti,
          foto: '',
        });
      }
    }

    this.router.navigate([ 'checkout' ],
      {
        queryParams: {
          pesanan: JSON.stringify(this.pesanan)
        }
      }
    );
  }
}