import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform, ToastController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { PesananService } from '../../../services/pesanan.service';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http/ngx';

declare var google;
import * as moment from 'moment';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {

  public map;
  @ViewChild('mapElement') mapElement;

  pesanan: any = [];
  total_bayar: number = 0;

  lat: any = 0;
  lng: any = 0;

  catatan: string = '';

  sec: number = 0;

  constructor(
    public geolocation: Geolocation,
    public http: HTTP,
    public pesananService: PesananService,
    public platform: Platform,
    public route: ActivatedRoute,
    public router: Router,
    public storage: Storage,
    public toastCtrl: ToastController,
    public zone: NgZone,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.pesanan = JSON.parse(params.pesanan);

      for (let i = 0; i < this.pesanan.length; i++) {
        this.total_bayar += this.pesanan[i].harga_jual * this.pesanan[i].kuantiti
      }
    });
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.geolocation.getCurrentPosition().then((resp) => {

        let map = new google.maps.Map(this.mapElement.nativeElement, {
          center: { lat: resp.coords.latitude, lng: resp.coords.longitude },
          disableDefaultUI: true,
          draggable: true,
          zoom: 18
        });

        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;

        //-----
        this.zone.run(async () => {
          this.getMapInfo(map.getCenter().lat(), map.getCenter().lng())

          google.maps.event.addListener(map, 'dragend', function () {
            var latLng = new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng());

            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'location': latLng }, function (results, status) {
              if (status !== 'OK') { alert('Geocoder failed due to: ' + status); return; }
              document.getElementById('alamat_geo').innerHTML = results[0].formatted_address;
            });
          });
        });
      }).catch((error) => {
        console.error('Error getting location', error);
      });


    });
  }

  getMapInfo(val_lat, val_lng) {
    this.lat = val_lat;
    this.lng = val_lng;

    var latLng = new google.maps.LatLng(val_lat, val_lng);

    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({ 'location': latLng }, function (results, status) {
      if (status !== 'OK') { alert('Geocoder failed due to: ' + status); return; }
      document.getElementById('alamat_geo').innerHTML = results[0].formatted_address;
    });
  }

  checkout() {
    this.storage.get('akun').then((akun) => {

      let data_test: any = {
        id_pelanggan: akun.id,
        nota: moment().format('YYMMDDHHmmss'),
        waktu: moment().format('DD-MM-YYYY HH:mm'),
        timestamp: moment().format('YYYY-MM-DD HH:mm:ss'),
        catatan: this.catatan,
        status: 'Pending',
        lokasi: {
          latitude: this.lat,
          longitude: this.lng,
          alamat: document.getElementById('alamat_geo').innerHTML
        },
        sub: this.pesanan,
        total_bayar: this.total_bayar,
      }

      this.pesananService.addPesanan(data_test).then((snap) => {
        // this.toast_success('Pemesanan Berhasil. \nPesanan akan diantar ke rumah anda.');

        // this.router.navigateByUrl('/tabs/pesanan');

        this.http.setDataSerializer('json');
        
        let body = {
          "notification": {
            "body": "Notification sent from APP"
          },
          "to": "dGqnxD4zSxatb-RWOJfswd:APA91bFwf_mDEKvn3xEbjIMa-VDgFE4ujUiaDVw34d_RPrYG4Gafu75AW834QJg20gvKreRtD2NiQV8R-f9Z51_aie5e5swvJ_0YlFcDRn4-AGkMUEPeICS3r1FHFNlvr40lnSgKT7RR"
        }

        let headers = {
          'Content-Type': 'application/json',
          'Authorization': 'key=AAAABZetYog:APA91bFCkUJa0zfnU47G4f7NcMvSiFLQfg5pIQpW8uanudWDFcDoGqwawR0jNype309sw41_lYfu1Vj7byXUkRiYSaRZ2QmZh9AxQcuUeBLr1jP0-1HfZVDkMILzrqnP9ZQ8Ou124FTw'
        }

        // this.http.setHeader('*', 'Accept', 'application/json');
        this.http.setHeader('*', 'Content-Type', 'application/json');
        this.http.setHeader('*', 'Authorization', 'key=AAAABZetYog:APA91bFCkUJa0zfnU47G4f7NcMvSiFLQfg5pIQpW8uanudWDFcDoGqwawR0jNype309sw41_lYfu1Vj7byXUkRiYSaRZ2QmZh9AxQcuUeBLr1jP0-1HfZVDkMILzrqnP9ZQ8Ou124FTw');

        this.http.post('https://fcm.googleapis.com/fcm/send', body, {}).then(data => {
          console.log('data1 : ' + data.status);
          console.log('data2 : ' + data.data); // data received by server
          console.log('data3 : ' + JSON.stringify(data.headers));
          console.log('data4 : ' + JSON.stringify(data));
        }).catch(error => {
          console.log('err1 : ' + error.status);
          console.log('err2 : ' + error.error); // error message as string
          console.log('err3 : ' + error.headers);
        });
      }, err => {
        // this.showToast('Gagal menambahkan data.');
        console.error(JSON.stringify(err));
        alert(JSON.stringify(err));
      });

    });
  }

  toast_success(msg) {
    this.toastCtrl.create({
      message: msg,
      position: 'top',
      color: 'success',
      buttons: [
        {
          text: 'X',
          role: 'cancel',
          handler: () => {
            console.log('Close clicked');
          }
        }
      ]
    }).then(toast => toast.present());
  }

}
