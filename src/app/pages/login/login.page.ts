import { Component, OnInit } from '@angular/core';
import { AkunService } from '../../services/akun.service';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

import * as CryptoJS from 'crypto-js';

export interface Akun {
  id?: string,
  nama: string,
  nomor_hp: string,
  password: string,
  foto: string
}

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: [ './login.page.scss' ],
})
export class LoginPage implements OnInit {

  public akunCollection: AngularFirestoreCollection<Akun>;

  form_login: any = {
    nomor_hp: '',
    password: '',
  };

  constructor(
    public afs: AngularFirestore,
    public akunService: AkunService,
    public router: Router,
    public storage: Storage,
  ) {
    this.akunCollection = this.afs.collection<Akun>('akun');
    // this.akun = this.akunCollection.snapshotChanges().pipe(
    //   map(actions => {
    //     return actions.map(a => {
    //       const data = a.payload.doc.data();
    //       const id = a.payload.doc.id;
    //       return { id, ...data };
    //     });
    //   })
    // );
  }

  ngOnInit() {
  }

  // login() {
  //   this.akunService.getAkun1('tist').subscribe(snap => {
  //     // alert(snap);
  //   })
  // }

  login() {
    this.akunCollection.ref.where('nomor_hp', '==', this.form_login.nomor_hp).get().then((result) => {
      result.forEach(doc => {
        // console.log(doc.data());

        var bytes = CryptoJS.AES.decrypt(doc.data().password.toString(), '');
        var plaintext = bytes.toString(CryptoJS.enc.Utf8);

        if (this.form_login.password == plaintext) {
          console.info('Auth Success');
          console.info(doc.data());
          console.info(doc.id);

          this.storage.set('akun', {
            id: doc.id,
            nama: doc.data().nama,
            nomor_hp: doc.data().nomor_hp,
            password: doc.data().password,
          });

          this.akunService.pubAkun({});
          this.router.navigateByUrl('/tabs');
        } else {
          console.info('Auth Failed');
        }
      })
    })
  }

}
