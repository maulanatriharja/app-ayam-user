import { Component, OnInit } from '@angular/core';
import { AkunService } from '../../../services/akun.service';
import { ToastController } from '@ionic/angular';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-registrasi',
  templateUrl: './registrasi.page.html',
  styleUrls: [ './registrasi.page.scss' ],
})
export class RegistrasiPage implements OnInit {

  registrasi: any = {
    nama: '',
    nomor_hp: '',
    password: '',
    password_konfirmasi: '',
    foto: ''
  };

  constructor(
    public akunService: AkunService,
    public toastCtrl: ToastController,
  ) { }

  ngOnInit() {

  }

  register() {
    let reg_form = {
      nama: this.registrasi.nama,
      nomor_hp: this.registrasi.nomor_hp,
      password: CryptoJS.AES.encrypt(this.registrasi.password, '').toString(),
      foto: ''
    };

    this.akunService.addAkun(reg_form).then(() => {
      this.showToast('Registrasi Berhasil.');
    }, err => {
      this.showToast('Gagal Registrasi.');
      console.error(JSON.stringify(err));
    });
  }

  showToast(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top',
    }).then(toast => toast.present());
  }

}
