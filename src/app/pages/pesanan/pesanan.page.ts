import { Component, OnInit } from '@angular/core';
import { PesananService } from '../../services/pesanan.service';

@Component({
  selector: 'app-pesanan',
  templateUrl: './pesanan.page.html',
  styleUrls: [ './pesanan.page.scss' ],
})
export class PesananPage implements OnInit {

  data: any = [];
  sec:number=0;
  constructor(
    public pesananService: PesananService,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.data = [];
    this.pesananService.getpesanan().subscribe(snap => {
      this.data = snap;
    });
  }

}
