import { Component, OnInit } from '@angular/core';
import { PesananService } from '../../../services/pesanan.service';

@Component({
  selector: 'app-pesanan-riwayat',
  templateUrl: './pesanan-riwayat.page.html',
  styleUrls: ['./pesanan-riwayat.page.scss'],
})
export class PesananRiwayatPage implements OnInit {

  data: any = [];
  sec:number=0;
  constructor(
    public pesananService: PesananService,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.data = [];
    this.pesananService.getpesanan().subscribe(snap => {
      this.data = snap;
    });
  }

}
