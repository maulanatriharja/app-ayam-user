import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PesananRiwayatPage } from './pesanan-riwayat.page';

describe('PesananRiwayatPage', () => {
  let component: PesananRiwayatPage;
  let fixture: ComponentFixture<PesananRiwayatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesananRiwayatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PesananRiwayatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
