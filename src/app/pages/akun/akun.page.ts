import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AkunService } from '../../services/akun.service';
import { AlertController, ToastController } from '@ionic/angular';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-akun',
  templateUrl: './akun.page.html',
  styleUrls: [ './akun.page.scss' ],
})
export class AkunPage implements OnInit {

  akun: any = {
    id: '',
    nama: '',
    nomor_hp: '',
    password: '',
  }

  constructor(
    public akunService: AkunService,
    public alertController: AlertController,
    public router: Router,
    public storage: Storage,
    public toastCtrl: ToastController,
  ) { }

  ngOnInit() {
    this.storage.get('akun').then((val) => {
      this.akun.id = val.id;
      this.akun.nama = val.nama;
      this.akun.nomor_hp = val.nomor_hp;
      this.akun.password = val.password;
    });
  }

  async editNama(val) {
    const alert = await this.alertController.create({
      header: 'Edit Nama',
      inputs: [
        {
          name: 'nama',
          type: 'text',
          id: 'nama',
          value: val,
          placeholder: 'Nama lengkap'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Simpan',
          handler: (val) => {
            console.log('Confirm Ok');

            this.akun.nama = val.nama;

            this.akunService.updateAkun(this.akun).then((res) => {
              console.info(res);

              this.storage.set('akun', {
                id: this.akun.id,
                nama: val.nama,
                nomor_hp: this.akun.nomor_hp,
              });
            }, err => {
              //this.showToast('Gagal mengupdate data.');
              console.error(JSON.stringify(err));
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async editNomorHP(val) {
    const alert = await this.alertController.create({
      header: 'Edit Nomor HP',
      inputs: [
        {
          name: 'nomor_hp',
          type: 'text',
          id: 'nomor_hp',
          value: val,
          placeholder: 'Nomor HP'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Simpan',
          handler: (val) => {
            console.log('Confirm Ok');

            this.akun.nomor_hp = val.nomor_hp;

            this.akunService.updateAkun(this.akun).then((res) => {
              console.info(res);

              this.storage.set('akun', {
                id: this.akun.id,
                nama: this.akun.nama,
                nomor_hp: val.nomor_hp,
              });
            }, err => {
              //this.showToast('Gagal mengupdate data.');
              console.error(JSON.stringify(err));
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async editPassword() {
    const alert = await this.alertController.create({
      header: 'Edit Nama',
      inputs: [
        {
          name: 'password_lama',
          type: 'text',
          id: 'password_lama',
          placeholder: 'Password Lama'
        },
        {
          name: 'password_baru',
          type: 'text',
          id: 'password_baru',
          placeholder: 'Password Baru'
        },
        {
          name: 'password_baru_konf',
          type: 'text',
          id: 'password_baru_konf',
          placeholder: 'Konfirmasi Password Baru'
        },
      ],
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (val) => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Simpan',
          handler: (val) => {
            console.log('Confirm Ok');

            var bytes = CryptoJS.AES.decrypt(this.akun.password, '');
            var curr_pass = bytes.toString(CryptoJS.enc.Utf8);

            if (curr_pass != val.password_lama) {
              this.toast_warning('Password Lama tidak cocok.');
              return false;
            } else if (!val.password_lama) {
              this.toast_warning('Silahkan isi Password Lama.');
              return false;
            } else if (!val.password_baru) {
              this.toast_warning('Silahkan isi Password Baru.');
              return false;
            } else if (!val.password_baru_konf) {
              this.toast_warning('Silahkan isi Konfirmasi Password Baru.');
              return false;
            } else if (val.password_baru != val.password_baru_konf) {
              this.toast_warning('Password Baru dan Konfirmasi Password Baru harus sama.');
              return false;
            } else {
              let pass: any = {
                id: this.akun.id,
                password: CryptoJS.AES.encrypt(val.password_baru, '').toString(),
              }

              this.akunService.updatePassword(pass).then((res) => {
                console.info(res);
              }, err => {
                //this.showToast('Gagal mengupdate data.');
                console.error(JSON.stringify(err));
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  toast_warning(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top',
      color: 'warning'
    }).then(toast => toast.present());
  }

  keluar() {
    this.storage.clear();
    this.akunService.pubAkun({});
    this.router.navigateByUrl('/tabs');
  }
}
