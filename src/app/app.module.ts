import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule, SETTINGS } from '@angular/fire/firestore';
import { Camera } from '@ionic-native/camera/ngx';
import { environment } from '../environments/environment';
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { registerLocaleData } from '@angular/common';
import localeId from '@angular/common/locales/id';
registerLocaleData(localeId);
import { HTTP } from '@ionic-native/http/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';


@NgModule({
  declarations: [ AppComponent ],
  entryComponents: [],
  imports: [
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    IonicStorageModule.forRoot(),
    AngularFirestoreModule,
    BrowserModule,
    IonicModule.forRoot({
      mode: 'ios'
    }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LOCALE_ID, useValue: 'id' },
    { provide: SETTINGS, useValue: {} },
    Camera,
    File,
    FilePath,
    Geolocation,
    HTTP,
    FirebaseX,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
